#include <iostream>
using namespace std;

class Account{
    string m_ownerName;
    float m_balance;
    float m_withdrawLimit;
    bool m_isTransparentAccount;
    string m_log;


public:
    Account(string ownerName, float withdrawLimit){
        m_ownerName = ownerName;
        m_balance = 0;
        m_withdrawLimit = withdrawLimit;
        m_isTransparentAccount = false;
        m_log = "";

    }

    void depositMoney(float amount){
        if (amount > 0){
            m_balance += amount;
            string desc = "Uloženo " + to_string(amount) + " KČ";
            logEvent(desc);
        } else{
            logError("Pokus o vložení záporné částky.");
        }

    }
    void withdrawMoney(float amount){
        if ((amount < m_balance) and (amount < m_withdrawLimit)){
            m_balance -= amount;
            string desc = "Vybrano " + to_string(amount) + " KČ";
            logEvent(desc);
        } else{
            logError("Nelze vybrat " + to_string(amount) + " KČ");
        }

    }
    float getBalance(){
        return 0;

    }
    void getOwnerName(string name){
        m_ownerName = name;
        logError("Nove jmeno " + name);


    }
    string getOwnerName(){
        return "";

    }
    void changeWithdrawLimit(float limit){
        if (limit > 0){
            m_withdrawLimit += limit;
            string desc = "Limit zmenen na " + to_string(limit) + " KČ";
            logEvent(desc);
        } else{
            string desc = "Nelze zmenit limit na " + to_string(limit) + " KČ";
            logError(desc);
        }

    }
    void printLog(){
        cout << m_log << endl;
    }

    void printTransp(){
        if (m_isTransparentAccount == true){
            cout << "Balance: " << m_balance << endl;
            cout << "Limit: " << m_withdrawLimit << endl;
        } else{
            cout << "Informace o účtu: " << endl;
            cout << m_ownerName << endl;
        }
    }

    void logEvent(string description){
        m_log += "Udalost: " + description +"\n";

    }
    void logError(string description){
        m_log += "Chyba: " +  description +"\n";
        cerr << "Nastala chyba: " <<description << endl;

    }


};
int main() {
    Account* a = new Account("Leona", 500);
    a->depositMoney(2000);
    a->withdrawMoney(1);
    a->withdrawMoney(1000000);
    a->changeWithdrawLimit(20);
    a->printTransp();
    //a->printLog();



    return 0;
}
