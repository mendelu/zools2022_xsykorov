//
// Created by xzejda on 22.04.2022.
//

#ifndef CV10ZAHRADNIK_ROSTLINA_H
#define CV10ZAHRADNIK_ROSTLINA_H


#include <string>

class Rostlina {
protected:
    float m_nakladyNaPorizeni;
    std::string m_nazev;
public:
    Rostlina(float nakladyNaPorizeni, std::string nazev);
    virtual void vypisRostlinu();
    virtual float getNakladyNaPestovani() = 0;
};


#endif //CV10ZAHRADNIK_ROSTLINA_H
