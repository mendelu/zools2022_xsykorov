//
// Created by xzejda on 22.04.2022.
//

#include <iostream>
#include "Strom.h"

Strom::Strom(float nakladyNaPorizeni, std::string nazev, int stari):
Rostlina(nakladyNaPorizeni, nazev) {
    m_stari = stari;
}

float Strom::getNakladyNaPestovani() {
    return m_nakladyNaPorizeni + (50 * m_stari);
}

void Strom::vypisRostlinu() {
    Rostlina::vypisRostlinu(); //volání metody vypisRoslinu předka
    using namespace std;
    cout << "Typ: Strom" << endl;
    cout << "Stari: " << m_stari << endl;
}
