//
// Created by xzejda on 22.04.2022.
//

#ifndef CV10ZAHRADNIK_ZAHRADA_H
#define CV10ZAHRADNIK_ZAHRADA_H

#include <vector>
#include "Rostlina.h"

class Zahrada {
    float m_rozpocet;
    std::vector<Rostlina*> m_rostliny;
public:
    Zahrada(float rozpocet);
    void pridatRostlinu(Rostlina* rostlina);
    void vypisRostlinyARozpocet();
    ~Zahrada();
};


#endif //CV10ZAHRADNIK_ZAHRADA_H
