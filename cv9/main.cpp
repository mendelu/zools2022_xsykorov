#include <iostream>
#include "Zahrada.h"
#include "Kvetina.h"
#include "Strom.h"

int main() {
    Rostlina* r1 = new Strom(50.0, "Lipa", 10);
    Rostlina* r2 = new Strom(25.0, "Olse", 5);
    Rostlina* r3 = new Kvetina(25.0, "Bledule", 255,255,255);
    Rostlina* r4 = new Kvetina(25.0, "Narcis", 255,255,0);

    Zahrada* zahrada = new Zahrada(10000);
    zahrada->pridatRostlinu(r1);
    zahrada->pridatRostlinu(r2);
    zahrada->pridatRostlinu(r3);
    zahrada->pridatRostlinu(r4);

    Zahrada* zahrada1 = new Zahrada(10000);
    zahrada1->pridatRostlinu(r1);
    zahrada1->pridatRostlinu(r2);
    zahrada1->pridatRostlinu(r3);
    zahrada1->pridatRostlinu(r4);
    zahrada->vypisRostlinyARozpocet();

    delete zahrada;

    zahrada1->vypisRostlinyARozpocet();

    delete zahrada1;
    return 0;
}
