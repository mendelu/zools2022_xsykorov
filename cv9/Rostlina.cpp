//
// Created by xzejda on 22.04.2022.
//

#include <iostream>
#include "Rostlina.h"

Rostlina::Rostlina(float nakladyNaPorizeni, std::string nazev) {
    m_nakladyNaPorizeni = nakladyNaPorizeni;
    m_nazev = nazev;
}

void Rostlina::vypisRostlinu() {
    using namespace std;
    cout << "***Rostlina***" << endl;
    cout << "Nazev: " << m_nazev << endl;
    cout << "Naklady na porizeni: " << m_nakladyNaPorizeni << endl;
}
