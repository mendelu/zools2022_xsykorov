//
// Created by xzejda on 22.04.2022.
//

#include <iostream>
#include "Kvetina.h"

Kvetina::Kvetina(float nakladyNaPorizeni, std::string nazev, int r,
                 int g, int b): Rostlina(nakladyNaPorizeni, nazev) {
    m_rgb.at(0) = r;
    m_rgb.at(1) = g;
    m_rgb.at(2) = b;
}
float Kvetina::getNakladyNaPestovani() {
    return m_nakladyNaPorizeni +
    (10 * m_rgb.at(0) + 1 * m_rgb.at(1) + 10 * m_rgb.at(2));
}
void Kvetina::vypisRostlinu() {
    Rostlina::vypisRostlinu(); //volání metody vypisRostlinu u předka
    using namespace std;
    cout << "Typ: Kvetina" << endl;
    cout << "Cervena: " << m_rgb.at(0) << endl;
    cout << "Zelena: " << m_rgb.at(1) << endl;
    cout << "Modra: " << m_rgb.at(2) << endl;
}
