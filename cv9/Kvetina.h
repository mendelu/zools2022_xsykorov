//
// Created by xzejda on 22.04.2022.
//

#ifndef CV10ZAHRADNIK_KVETINA_H
#define CV10ZAHRADNIK_KVETINA_H


#include <array>
#include "Rostlina.h"

class Kvetina: public Rostlina {
    std::array<int,3> m_rgb;
public:
    Kvetina(float nakladyNaPorizeni, std::string nazev,
            int r, int g, int b);
    float getNakladyNaPestovani() override;
    void vypisRostlinu() override;
};


#endif //CV10ZAHRADNIK_KVETINA_H
