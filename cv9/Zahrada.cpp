//
// Created by xzejda on 22.04.2022.
//

#include <iostream>
#include "Zahrada.h"

Zahrada::Zahrada(float rozpocet) {
    m_rozpocet = rozpocet;
}
void Zahrada::pridatRostlinu(Rostlina *rostlina) {
    if (m_rozpocet >= rostlina->getNakladyNaPestovani()) {
        m_rozpocet -= rostlina->getNakladyNaPestovani();
        m_rostliny.push_back(rostlina);
    } else {
        std::cout << "Nedostatecny rozpocet!" << std::endl;
    }
}
void Zahrada::vypisRostlinyARozpocet() {
    using namespace std;
    cout << "***Zahrada***" << endl;
    cout << "Rozpocet: " << m_rozpocet << endl;
    cout << "***Rostiny na zahrade***" << endl;
    for (int i = 0; i < m_rostliny.size(); ++i) {
        m_rostliny.at(i)->vypisRostlinu();
    }
}
Zahrada::~Zahrada() {
    for (int i = 0; i < m_rostliny.size(); ++i) {
        delete m_rostliny.at(i);
    }
}
