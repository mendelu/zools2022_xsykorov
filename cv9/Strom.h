//
// Created by xzejda on 22.04.2022.
//

#ifndef CV10ZAHRADNIK_STROM_H
#define CV10ZAHRADNIK_STROM_H


#include "Rostlina.h"

class Strom: public Rostlina {
    int m_stari;
public:
    Strom(float nakladyNaPorizeni, std::string nazev, int stari);
    float getNakladyNaPestovani() override;
    void vypisRostlinu() override;
};


#endif //CV10ZAHRADNIK_STROM_H
