#include <iostream>
#include "Hero/Hero.h"
#include "Builder/BarbarianHeroBuilder.h"
#include "Builder/HeroDirector.h"
#include "Builder/RogueHeroBuilder.h"

int main() {
    Hero* hero = new Hero();
    HeroDirector* director = new HeroDirector(new RogueHeroBuilder());

    hero = director->constructHero();
    hero->printInfo();
    director->setHeroBuilder(new BarbarianHeroBuilder());
    hero = director->constructHero();
    hero->printInfo();



    delete hero, director;
    return 0;
}
