//
// Created by xsykorov on 29.04.2022.
//

#include "BarbarianHeroBuilder.h"

BarbarianHeroBuilder::BarbarianHeroBuilder() {

}

void BarbarianHeroBuilder::setAttributes() {
    m_hero->setHealth(100);
    m_hero->setStrength(10);
    m_hero->setAgility(5);
    m_hero->setIntelligence(5);

}

void BarbarianHeroBuilder::setArmor() {
    m_hero->setArmor(new Armor("Barbarian armor", 100));

}

void BarbarianHeroBuilder::setItems() {
    m_hero->addItem(new Item("Health potion", 1, 50));
    m_hero->addItem(new Item("Strenght potion", 2, 10));
    m_hero->addItem(new Item("Barbarian neckle", 0, 2000));

}

void BarbarianHeroBuilder::setWeapons() {
    m_hero->setWeapon(new Weapon("Barbarian sword", 50));

}
