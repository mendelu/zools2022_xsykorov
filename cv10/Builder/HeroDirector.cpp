//
// Created by xsykorov on 29.04.2022.
//

#include "HeroDirector.h"

HeroDirector::HeroDirector(HeroBuilder *heroBuilder) {
    m_heroBuilder = heroBuilder;

}

void HeroDirector::setHeroBuilder(HeroBuilder *heroBuilder) {
    m_heroBuilder = heroBuilder;

}

Hero *HeroDirector::constructHero() {
    m_heroBuilder->createHero();
    m_heroBuilder->setAttributes();
    m_heroBuilder->setWeapons();
    m_heroBuilder->setArmor();
    m_heroBuilder->setItems();

    return m_heroBuilder->getHero();
}
