//
// Created by xsykorov on 29.04.2022.
//

#include "HeroBuilder.h"

HeroBuilder::HeroBuilder() {
    m_hero = nullptr;

}

void HeroBuilder::createHero() {
    m_hero = new Hero();

}

Hero *HeroBuilder::getHero() {
    return m_hero;
}
