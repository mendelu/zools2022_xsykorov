//
// Created by xsykorov on 29.04.2022.
//

#ifndef CV9_BARBARIANHEROBUILDER_H
#define CV9_BARBARIANHEROBUILDER_H


#include "HeroBuilder.h"

class BarbarianHeroBuilder: public HeroBuilder {
public:
    BarbarianHeroBuilder();
    void setAttributes() override;
    void setArmor() override;
    void setItems() override;
    void setWeapons() override;

};


#endif //CV9_BARBARIANHEROBUILDER_H
