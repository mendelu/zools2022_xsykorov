//
// Created by xsykorov on 29.04.2022.
//

#ifndef CV9_HERODIRECTOR_H
#define CV9_HERODIRECTOR_H


#include "HeroBuilder.h"

class HeroDirector {
    HeroBuilder* m_heroBuilder;
public:
    HeroDirector(HeroBuilder* heroBuilder);
    void setHeroBuilder(HeroBuilder* heroBuilder);
    Hero* constructHero();


};


#endif //CV9_HERODIRECTOR_H
