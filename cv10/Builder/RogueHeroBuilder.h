//
// Created by xsykorov on 29.04.2022.
//

#ifndef CV9_ROGUEHEROBUILDER_H
#define CV9_ROGUEHEROBUILDER_H


#include "HeroBuilder.h"

class RogueHeroBuilder: public HeroBuilder{
    void setAttributes() override;
    void setWeapons() override;
    void setArmor() override;
    void setItems() override;


public:
    RogueHeroBuilder();
};


#endif //CV9_ROGUEHEROBUILDER_H
