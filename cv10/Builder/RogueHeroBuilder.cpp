//
// Created by xsykorov on 29.04.2022.
//

#include "RogueHeroBuilder.h"

RogueHeroBuilder::RogueHeroBuilder() {

}

void RogueHeroBuilder::setAttributes() {
    m_hero->setHealth(80);
    m_hero->setStrength(5);
    m_hero->setAgility(10);
    m_hero->setIntelligence(5);

}

void RogueHeroBuilder::setWeapons() {
    m_hero->setWeapon(new Weapon("Rogue dagger", 20));

}

void RogueHeroBuilder::setArmor() {
    m_hero->setArmor(new Armor("Rogue armor", 50));

}

void RogueHeroBuilder::setItems() {
    m_hero->addItem(new Item("Health potion", 1, 50));
    m_hero->addItem(new Item("Agility potion", 2, 10));
    m_hero->addItem(new Item("Rogue neckle", 0, 2000));
}
