//
// Created by xzejda on 01.12.2021.
//

#ifndef CV9_WEAPON_H
#define CV9_WEAPON_H


#include <string>

class Weapon {
    std::string m_name;
    int m_damage;
public:
    Weapon(std::string name, int damage);
    std::string getName();
    int getDamage();
};


#endif //CV9_WEAPON_H
