//
// Created by xzejda on 01.12.2021.
//

#include "Armor.h"

Armor::Armor(std::string name, int defencePoints) {
    m_name = name;
}

std::string Armor::getName() {
    return m_name;
}

int Armor::getDefencePoints() {
    return m_defencePoints;
}
