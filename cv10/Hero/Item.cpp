//
// Created by xzejda on 01.12.2021.
//

#include "Item.h"

Item::Item(std::string name, int bonusType, int bonusValue) {
    m_name = name;
    m_bonusType = bonusType;
    m_bonusValue = bonusValue;
}

std::string Item::getName() {
    return m_name;
}

int Item::getBonusType() {
    return m_bonusType;
}

int Item::getBonusValue() {
    return m_bonusValue;
}
