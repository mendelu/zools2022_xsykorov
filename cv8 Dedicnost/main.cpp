#include <iostream>
using namespace std;

class Otec {
protected:
    string m_jmeno;
public:
    Otec(string jmeno) {
        m_jmeno = jmeno;
        cout << "Konstruktor otce" << endl;
    }
    /// virtualni metoda, volana vždy u potomka
    virtual void pozdrav() {
        cout << "Otec zdravi" << endl;
    }
    /// metoda, která se volá vždy u předka
    void soucet() {
        cout << "Otec scita" << endl;
    }
    /// čistě virtuální metoda - z celé třídy se stává ciste abstraktní třída a nemůžeme tvořit instance
    /// metodu musíme definovat v potomcích
    virtual void dejDarek() = 0;

    virtual ~Otec() {
        cout << "Destruktor otce" << endl;
    }
};

class Syn: public Otec {
    int m_pocetAuticek;
public:
    Syn(string jmeno, int pocetAuticek): Otec(jmeno) {
        m_pocetAuticek = pocetAuticek;
        cout << "konstruktor syna" << endl;
    }
    void pozdrav() {
        cout << "Syn zdravi" << endl;
    }
    void soucet() {
        cout << "Syn scita" << endl;
    }
    void dejDarek() override {
        cout << "Syn dava darek" << endl;
    }
};

class Dcera: public Otec {
    int m_pocetPanenek;
public:
    Dcera(string jmeno, int pocetPanenek): Otec(jmeno) {
        m_pocetPanenek = pocetPanenek;
        cout << "konstruktor dcery" << endl;
    }
    void dejDarek() override {
        cout << "Dcera dava darek" << endl;
        cout << "Dcera dava prachy" << endl;
    }
    ~Dcera() {
        cout << "Destruktor dcery" << endl;
    }
};

int main() {
    Otec* x1 = new Dcera("David", 5);
    x1->pozdrav();
    x1->soucet();
    x1->dejDarek();

    delete x1;
    return 0;
}
