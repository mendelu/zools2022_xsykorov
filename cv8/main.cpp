#include <iostream>

#include "Administrativa.h"
#include "Zamestnanec.h"
#include "Programator.h"
#include "Manazer.h"



int main() {
    Zamestnanec* zamestnanec
    = new Zamestnanec("Jan",new Programator(5));

    zamestnanec->setPrace(new Manazer(3));

    zamestnanec->getPrace()->printInfo();

    delete zamestnanec;
    return 0;
}
