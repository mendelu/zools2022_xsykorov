//
// Created by xzejda on 08.04.2022.
//

#include <iostream>
#include "Prace.h"

Prace::Prace(int pocetLetNaPozici) {
    m_pocetLetNaPozici = pocetLetNaPozici;
}

void Prace::printInfo() {
    using namespace std;
    cout << "Pocet let na pozici: " << m_pocetLetNaPozici << endl;
    cout << "Mzda: " << vratPlat() << endl;
}
