//
// Created by xzejda on 08.04.2022.
//

#ifndef CV8ZAMESTNANCI_ADMINISTRATIVA_H
#define CV8ZAMESTNANCI_ADMINISTRATIVA_H


#include "Prace.h"

class Administrativa: public Prace {
public:
    Administrativa(int pocetLetNaPozici);
    int vratPlat();
};


#endif //CV8ZAMESTNANCI_ADMINISTRATIVA_H
