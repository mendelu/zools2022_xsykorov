//
// Created by xzejda on 08.04.2022.
//

#include <iostream>
#include "Zamestnanec.h"

Zamestnanec::Zamestnanec(std::string jmeno, Prace* prace) {
    m_jmeno = jmeno;
    m_prace = prace;
}

Zamestnanec::~Zamestnanec() {
    delete m_prace;
}

void Zamestnanec::setPrace(Prace *prace) {
    delete m_prace;
    m_prace = prace;
}

Prace *Zamestnanec::getPrace() {
    return m_prace;
}


