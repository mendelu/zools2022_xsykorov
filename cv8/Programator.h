//
// Created by xzejda on 08.04.2022.
//

#ifndef CV8ZAMESTNANCI_PROGRAMATOR_H
#define CV8ZAMESTNANCI_PROGRAMATOR_H


#include "Prace.h"

class Programator: public Prace {
public:
    Programator(int pocetLetNaPozici);
    int vratPlat() override;
};


#endif //CV8ZAMESTNANCI_PROGRAMATOR_H
