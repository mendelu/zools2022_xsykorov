//
// Created by xzejda on 08.04.2022.
//

#ifndef CV8ZAMESTNANCI_PRACE_H
#define CV8ZAMESTNANCI_PRACE_H


#include <string>

class Prace {
protected:
    int m_pocetLetNaPozici;
public:
    Prace(int pocetLetNaPozici);
    virtual int vratPlat() = 0;
    void printInfo();
};


#endif //CV8ZAMESTNANCI_PRACE_H
