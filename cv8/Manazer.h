//
// Created by xzejda on 08.04.2022.
//

#ifndef CV8ZAMESTNANCI_MANAZER_H
#define CV8ZAMESTNANCI_MANAZER_H


#include "Prace.h"

class Manazer: public Prace {
public:
    Manazer(int pocetLetNaPozici);
    int vratPlat();
};


#endif //CV8ZAMESTNANCI_MANAZER_H
