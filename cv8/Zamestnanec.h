//
// Created by xzejda on 08.04.2022.
//

#ifndef CV8ZAMESTNANCI_ZAMESTNANEC_H
#define CV8ZAMESTNANCI_ZAMESTNANEC_H


#include <string>
#include <vector>
#include "Prace.h"

class Zamestnanec {
    std::string m_jmeno;
    Prace* m_prace;
public:
    Zamestnanec(std::string jmeno, Prace* prace);
    void setPrace(Prace* prace);
    Prace* getPrace();
    ~Zamestnanec();
};


#endif //CV8ZAMESTNANCI_ZAMESTNANEC_H
