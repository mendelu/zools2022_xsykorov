#include <iostream>
using namespace std;

class Player{
public:
    string m_name;
    int m_hp;
    int m_mana;

    void setJmeno(string name){
        if(name == ""){
            cout << "Nemuzes nemit zadne jmeno." << endl;
        }
        else{
            m_name = name;
        }
    }
    void setHP(int hp){

        m_hp = hp;
    }
    void setMana(int mana){
        m_mana = mana;
    }

    string getJmeno(){
        return m_name;
    }
    int getHP(){
        if(m_hp <= 0){
            cout << "Tento hrac je mrtvy." << endl;
            return m_hp;
        }

     else{
        cout << "Tento hrac je zivy" << endl;
            return m_hp;
     }
    }


    int getMana(){
        return m_mana;
    }

    void printInfo(){
        cout << "Jmenuju se " << m_name << endl;
        cout << "Moje zivoty jsou " << m_hp << endl;
        cout << "Moje mana je " << m_mana << endl;
    }


};

int main() {
    Player* player = new Player();
    player->setJmeno("Lucio");
    player->setHP(-5);
    player->setMana(200);


    player->printInfo();

    delete player;
    return 0;
}
