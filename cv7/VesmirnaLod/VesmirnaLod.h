//
// Created by xsykorov on 01.04.2022.
//

#ifndef CV7_VESMIRNALOD_H
#define CV7_VESMIRNALOD_H


#include <string>
#include <vector>
#include <array>
#include "Nadrz.h"
#include "GravitacniPumpa.h"
#include "ZbranovySystem.h"
#include "../Planeta/Planeta.h"

class VesmirnaLod {
    std::string m_nazev;
    float m_souradniceX;
    float m_souradniceY;
    float m_spotrebaPalivaNaMldKm;
    Nadrz *m_nadrz;
    std::vector<GravitacniPumpa*> m_gravitacniPumpy;
    std::array<ZbranovySystem*,3> m_zbranoveSystemy;

public:
    VesmirnaLod(std::string nazev, float souradniceX, float souradniceY,
                float spotrebaPalivaNaMldKm, float kapacitaNadrze);
    void printInfo();
    void InformaceOPlanete(Planeta* planeta);
    float getUsetreniPalivaZaMldKm();
    float getPoskozeniZaSekundu();
    float getVzdalenostLodiOdSouradnice(float souradniceX, float souradniceY);
    float getDoletovaVzdalenost();
    float getSpotrebaPalivaZaCestu(float pocetMldKm);
    void letNaPlanetu(Planeta* planeta);

};


#endif //CV7_VESMIRNALOD_H
