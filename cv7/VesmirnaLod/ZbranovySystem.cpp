//
// Created by xsykorov on 01.04.2022.
//

#include "ZbranovySystem.h"


ZbranovySystem::ZbranovySystem(float vystrelyZaSekundu, float poskozeniJedneStrely) {
    m_vystrelyZaSekundu = vystrelyZaSekundu;
    m_poskozeniJedneStrely = poskozeniJedneStrely;


}

float ZbranovySystem::getPoskozeniZaSekundu() {
    return m_poskozeniJedneStrely * m_vystrelyZaSekundu;
}
