//
// Created by xsykorov on 01.04.2022.
//

#ifndef CV7_ZBRANOVYSYSTEM_H
#define CV7_ZBRANOVYSYSTEM_H


class ZbranovySystem {
    float m_vystrelyZaSekundu;
    float m_poskozeniJedneStrely;

public:
    ZbranovySystem(float vystrelyZaSekundu, float poskozeniJedneStrely);
    float getPoskozeniZaSekundu();

};


#endif //CV7_ZBRANOVYSYSTEM_H
