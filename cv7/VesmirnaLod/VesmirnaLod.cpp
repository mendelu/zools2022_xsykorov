//
// Created by xsykorov on 01.04.2022.
//

#include <iostream>
#include "VesmirnaLod.h"

VesmirnaLod::VesmirnaLod(std::string nazev, float souradniceX, float souradniceY, float spotrebaPalivaNaMldKm,
                         float kapacitaNadrze) {
    m_nazev = nazev;
    m_souradniceX = souradniceX;
    m_souradniceY = souradniceY;
    m_spotrebaPalivaNaMldKm = spotrebaPalivaNaMldKm;
    m_nadrz = new Nadrz(kapacitaNadrze, kapacitaNadrze);

    for (int i = 0; i < m_zbranoveSystemy.size(); ++i) {
        m_zbranoveSystemy.at(i)= nullptr;

    }

}

void VesmirnaLod::printInfo() {
    std::cout << "Nazev lodi" << m_nazev << "\n";
    std::cout <<"Souradnice " << m_souradniceX << ", " << m_souradniceY << "\n";
    std::cout << "Spotreba paliva " << m_spotrebaPalivaNaMldKm << "\n";
    std::cout << "Nadrz" << m_nadrz << std::endl;
    std::cout << "Kapacita paliva" << m_nadrz << std::endl;


}

void VesmirnaLod::InformaceOPlanete(Planeta* planeta) {

}

float VesmirnaLod::getUsetreniPalivaZaMldKm() {
    return 0;
}

float VesmirnaLod::getPoskozeniZaSekundu() {
    return 0;
}

float VesmirnaLod::getVzdalenostLodiOdSouradnice(float souradniceX, float souradniceY) {
    return 0;
}

float VesmirnaLod::getDoletovaVzdalenost() {
    return 0;
}

float VesmirnaLod::getSpotrebaPalivaZaCestu(float pocetMldKm) {
    return 0;
}

void VesmirnaLod::letNaPlanetu(Planeta *planeta) {

}
