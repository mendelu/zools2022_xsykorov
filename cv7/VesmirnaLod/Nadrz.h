//
// Created by xsykorov on 01.04.2022.
//

#ifndef CV7_NADRZ_H
#define CV7_NADRZ_H


class Nadrz {
    float m_kapacitaPaliva;
    float m_aktualniMnozstviPaliva;

public:
    Nadrz(float kapacitaPaliva, float aktualniMnozstviPaliva);
    void setAktualniMnozstviPaliva(float mnozstviPaliva);
    float getKapacitaPaliva();

};


#endif //CV7_NADRZ_H
