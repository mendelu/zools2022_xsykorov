//
// Created by xsykorov on 01.04.2022.
//

#include <iostream>
#include "Nadrz.h"

Nadrz::Nadrz(float kapacitaPaliva, float aktualniMnozstviPaliva) {
    m_aktualniMnozstviPaliva = aktualniMnozstviPaliva;
    m_kapacitaPaliva = kapacitaPaliva;
}

void Nadrz::setAktualniMnozstviPaliva(float mnozstviPaliva) {
    if (m_aktualniMnozstviPaliva +mnozstviPaliva <= m_kapacitaPaliva){
        m_aktualniMnozstviPaliva += mnozstviPaliva;
    } else{
        std::cout << "Neodstatecna kapacita nadrze!" << std::endl;
    }

}

float Nadrz::getKapacitaPaliva() {
    return m_kapacitaPaliva;
}

