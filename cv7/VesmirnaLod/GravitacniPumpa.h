//
// Created by xsykorov on 01.04.2022.
//

#ifndef CV7_GRAVITACNIPUMPA_H
#define CV7_GRAVITACNIPUMPA_H


class GravitacniPumpa {
    float m_usetreniPalivaNaMldKm;

public:
    GravitacniPumpa(float usetreniPalivaNaMldKm);
    float getUsetreniPalivaNaMldKm();

};


#endif //CV7_GRAVITACNIPUMPA_H
