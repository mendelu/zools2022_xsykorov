//
// Created by xsykorov on 01.04.2022.
//

#include "GravitacniPumpa.h"

GravitacniPumpa::GravitacniPumpa(float usetreniPalivaNaMldKm) {
    m_usetreniPalivaNaMldKm = usetreniPalivaNaMldKm;
}

float GravitacniPumpa::getUsetreniPalivaNaMldKm() {
    return m_usetreniPalivaNaMldKm;
}
