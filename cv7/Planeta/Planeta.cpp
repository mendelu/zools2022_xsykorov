//
// Created by xsykorov on 01.04.2022.
//

#include "Planeta.h"

Planeta::Planeta(float souradniceX, float souradniceY, std::string nazev) {
    m_nazev = nazev;
    m_souradniceX = souradniceX;
    m_souradniceY = souradniceY;
}

float Planeta::getSouradniceX() {
    return m_souradniceX;
}

float Planeta::getSouradniceY() {
    return m_souradniceY;
}

std::string Planeta::getNazev() {
    return m_nazev;
}
