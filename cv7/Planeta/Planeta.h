//
// Created by xsykorov on 01.04.2022.
//

#ifndef CV7_PLANETA_H
#define CV7_PLANETA_H


#include <string>

class Planeta {
public:
    Planeta(const std::string &mNazev);

private:
    float m_souradniceX;
    float m_souradniceY;
    std::string m_nazev;

public:
    Planeta(float souradniceX, float souradniceY, std::string nazev);
    float getSouradniceX();
    float getSouradniceY();
    std::string getNazev();

};


#endif //CV7_PLANETA_H
