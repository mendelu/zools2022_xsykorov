#include <iostream>
#include <vector>

using namespace std;

class Kurz{
    string m_nazev;
    int m_cena;

public:
    Kurz(string nazev, int cena){
        m_nazev = nazev;
        m_cena = cena;
    }
    string getNazev(){
        return m_nazev;
    }
};


class Student{
    string m_jmeno;
    int m_rokNarozeni;
    string m_bydliste;

public:
    Student(string jmeno, int rokNarozeni, string bydliste){
        m_jmeno = jmeno;
        m_rokNarozeni = rokNarozeni;
        m_bydliste = bydliste;
    }

    Student(string jmeno, int rokNarozeni):
    Student(jmeno, rokNarozeni, "Bydliste nezname"){
    }
    string getJmeno(){
        return m_jmeno;
    }
};

class Zapis{
    Student* m_student;
    Kurz* m_kurz;
    string m_znamka;

public:
    Zapis(Student* student, Kurz* kurz){
        m_student = student;
        m_kurz = kurz;
        m_znamka = "";
    }
    void setZnamka(string znamka){
        m_znamka = znamka;
    }
    void printInfo(){
        cout << "Student " << m_student->getJmeno()<<
        " je zapsany do kurzu " << m_kurz->getNazev()<<
        " znamka je " << m_znamka << endl;
    }
    Student* getStudent(){
        return m_student;
    }
    Kurz* getKurz(){
        return m_kurz;
    }
};

class EvidenceZapisu{
    vector<Zapis*> m_zapisy;

public:
    EvidenceZapisu(){

    }
    void pridejZapis(Zapis* zapis){
        m_zapisy.push_back(zapis);
    }
    void vypisVsechnyZnamky(){
        cout << "Vypis vsechny znamky" << endl;
        for (int i = 0; i < m_zapisy.size(); ++i) {
            m_zapisy.at(i)->printInfo();
        }
    }
    void vypisZnamkyStudenta(string jmeno){
        cout << "Vypis vsechny znamky studenta" << endl;
        for (int i = 0; i < m_zapisy.size(); ++i) {
            if (m_zapisy.at(i)->getStudent()->getJmeno() == jmeno){
                m_zapisy.at(i)->printInfo();
            }

        }

    }

    void vypisZnamkyKurzu(string nazev){
        cout << "Vypis znamky kurzu" << endl;
        for (int i = 0; i < m_zapisy.size(); ++i) {
            if (m_zapisy.at(i)->getKurz()->getNazev() == nazev){
                m_zapisy.at(i)->printInfo();
            }

        }
    }
};

int main() {
    Student* student = new Student("David", 1994, "Brno");
    Kurz* kurz = new Kurz("ZOO", 0);
    Student* student1 = new Student("Jana", 1995, "Praha");
    Kurz* kurz1 = new Kurz("ALG", 100);
    Zapis* zapis = new Zapis(student, kurz);
    Zapis* zapis2 = new Zapis(student, kurz1);
    Zapis* zapis3 = new Zapis(student1, kurz);
    Zapis* zapis4 = new Zapis(student1, kurz1);

    EvidenceZapisu* evidenceZapisu = new EvidenceZapisu();
    evidenceZapisu->pridejZapis(zapis);
    evidenceZapisu->pridejZapis(zapis2);
    evidenceZapisu->pridejZapis(zapis3);
    evidenceZapisu->pridejZapis(zapis4);

    zapis->setZnamka("A");
    zapis2->setZnamka("B");
    zapis3->setZnamka("D");

    //evidenceZapisu->vypisVsechnyZnamky();
    evidenceZapisu->vypisZnamkyStudenta("David");
    evidenceZapisu->vypisZnamkyKurzu("ALG");


    delete student, kurz, zapis, kurz1, student1, zapis2, zapis3, zapis4, evidenceZapisu;

    return 0;
}
