#include <iostream>
using namespace std;

class ErrorLog{
    static ErrorLog* s_log;
    string m_errors;
    ErrorLog(){
        m_errors = "***ERROR LOGGER***";
    }

public:
    static ErrorLog* getErrorLog(){
        if (s_log == nullptr){
            s_log = new ErrorLog();
        }
        return s_log;
    }

    void logError(string where, string what){
        m_errors += where + what + "\n";
    }
    string  getErrors(){
        return m_errors;
    }


};
ErrorLog* ErrorLog::s_log = nullptr;

int main() {
    cout << ErrorLog::getErrorLog()->getErrors();
    ErrorLog::getErrorLog()->logError("Main", "Instance");


    return 0;
}
